/**
 * Created by ebertoia on 13/12/18.
 */
var Controles = {
    airlines : {},
    reservations: {},
    destinations:  [{ id: "MIA", name: "Miami" }, {id: "BUE", name:"Buenos Aires"}]

};
(function ($, c) {

    c.cnt_getAllAirlines = function () {
        $.ajax({
            url: 'action.php',
            type: 'post',
            async: true,
            data: 'getAllAirlines',
            success: function (data) {
                c.addAirlinesSelect(data)
                c.setAirlines(data)
            },
            error: c.error
        });
    }

    c.cnt_getAllReservation = function () {
        $.ajax({
            url: 'action.php',
            type: 'post',
            async: true,
            data: 'getAllReservation',
            success: function (data){
                c.addReservation(data)
            },
            error: c.error
        });
    }

    c.dataProcessor = function (data) {
        console.log(data)
    }

    c.saveReservation = function (element) {
        element.bind('submit', function () {
            $.ajax({
                type: 'post',
                url: 'action.php',
                data: $('form').serialize()+'&saveReservation',
                success: function (data) {
                    var reservation = []
                    reservation.push(data)
                    c.addReservation(reservation);
                }
            });
            return false;
        });
    }

    c.setAirlines = function (ev){
        localStorage.setItem("airlines", JSON.stringify(ev));
        c.airlines = JSON.parse(localStorage.getItem("airlines"))
    }


    c.loadAirlines = function() {
        if (localStorage.getItem("airlines") === null) {
            c.cnt_getAllAirlines()
        }
        c.airlines = JSON.parse(localStorage.getItem("airlines"))
        c.addAirlinesSelect(c.airlines)
    }


    c.addReservation = function(reservation) {
        $.each(reservation, function(key, val) {
            var body = '<tr>';
            body += '<td>'+reservation[key].id+'</td>';
            body += '<td>'+c.nameDestination(reservation[key].destination_from_id)+'</td>';
            body += '<td>'+c.dateFormatIso(reservation[key].date_from)+'</td>';
            body += '<td>'+c.nameDestination(reservation[key].destination_to_id)+'</td>';
            body += '<td>'+c.dateFormatIso(reservation[key].date_to)+'</td>';
            body += '<td>'+c.iataCode(reservation[key].airline_id)+'</td>';
            body += '<td>'+reservation[key].date_expiration+'</td>';
            body += '<tr>';

            $(".reservations").append(body);
        })
    }

    c.dateFormatIso = function (date){
        var dateToFormat = new Date(date)
        dateToFormat = dateToFormat.toISOString()
        return  dateToFormat
    }

    c.nameDestination = function (id) {
        return (c.destinations.find(item => item.id === id)).name
    }

    c.iataCode = function (id) {
        return (c.airlines.find(item => item.id === id)).iata

    }

    c.addAirlinesSelect = function(airlines) {
        $.each(airlines, function(key, val) {
            var option ='<option value="'+airlines[key].id+'">'+airlines[key].nombre+'</option>';
            $("#airlines").append(option);
        })
    }


    c.datePicker = function (element) {
        element.daterangepicker({
            timePicker: true,
            timePicker24Hour:true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'YYYY-MM-DD HH:mm'
            }
        });
    }

})(jQuery, Controles);

function init() {
    Controles.loadAirlines();

    Controles.datePicker($('input[name="datetimes"]'));
    Controles.cnt_getAllReservation();
    Controles.saveReservation($('form'))
}
$(document).ready(init);
