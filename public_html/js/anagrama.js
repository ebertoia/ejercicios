/**
 * Created by ebertoia on 13/12/18.
 */
var Anagrama = {


};
(function ($, a) {


    a.evaluateAnagrama = function (element) {
        element.bind('submit', function () {
            $.ajax({
                type: 'get',
                url: 'action.php',
                data: $('form').serialize(),
                success: function (data) {
                    a.addMessage(data);
                }
            });
            return false;
        });
    }

    a.addMessage = function(data) {

            var body = '<div class="alert alert-info">';
            body += data;
            body += '<div>';

            $(".responseAnagrama").html(body);

    }


})(jQuery, Anagrama);

function init() {
    Anagrama.evaluateAnagrama($('form'))
}
$(document).ready(init);
