<?php

class PracticeController extends ViewController
{
    protected $section = null;

    public function __construct()
    {

    }

    public function exampleOne()
    {
        include_once 'views/exampleOne.php';
    }

    public function exampleThree()
    {
        include_once 'views/exampleThree.php';
    }

}