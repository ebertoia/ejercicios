<?php
require_once('DataBase.php');

class Rules
{

    public function getAllRules()
    {
        try {
            $mysqli = DataBase::connex();
            $query = '
				    SELECT * FROM
				        rules_engine.business_rules';
            $result = $mysqli->query($query);

            while ($row = $result->fetch_assoc()) {
                $rule['id'] = $row['business_rules_id'];
                $rule['name'] = $row['name'];
                $rule['airline_id'] = $row['airline_id'];
                $rule['days'] = $row['days'];
                $rule['working_days'] = $row['working_days'];
                $rule['last_day'] = $row['last_day'];
                $rule['limit_day'] = $row['limit_day'];
                $rules[] = $rule;
            }
            $mysqli->query($query);
            $mysqli->close();
            return Common::encodingJson($rules);
        } catch (Exception $e) {
            echo 'Se produjo este error' . $e;
        }


    }

    public function getIdAirlineRules($id)
    {
        try {
            $mysqli = DataBase::connex();
            $query = '
				    SELECT * FROM
				        rules_engine.business_rules 
				        WHERE  rules_engine.business_rules.airline_id = '.$id;
            $result = $mysqli->query($query);
            while ($row = $result->fetch_assoc()) {
                $rule['id'] = $row['business_rules_id'];
                $rule['name'] = $row['name'];
                $rule['airline_id'] = $row['airline_id'];
                $rule['days'] = $row['days'];
                $rule['working_days'] = $row['working_days'];
                $rule['last_day'] = $row['last_day'];
                $rule['limit_day'] = $row['limit_day'];
                $rules[] = $rule;
            }
            $mysqli->query($query);
            $mysqli->close();
            return $rules;
        } catch (Exception $e) {
            echo 'Se produjo este error' . $e;
        }


    }


    public function applicationOfRules($reservations)
    {
        if(isset($reservations) && $reservations != "null" ){

            $rules = $this->getIdAirlineRules($reservations['airline_id']);

            foreach ($rules as $idRul => $rulValue) {
                $reservations['expirationDate'] = $this->setDateExpiration($rulValue, $reservations);
            }
            return $reservations['expirationDate'];
        }
    }

    public function setDateExpiration($rules, $reservation)
    {

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $date_reservation = strtotime($reservation['date_reservation']); //strtotime("2018-11-26");
        $dateExpiration = '';
        $i = 1;
        if(!$rules['last_day']){
            for ($date_reservation; $i <= (int)$rules['days']; $date_reservation = strtotime('+1 day ' . date('Y-m-d', $date_reservation))) {
                if($rules['working_days']){
                    if ((strcmp(date('D', $date_reservation), 'Sun') != 0) && (strcmp(date('D', $date_reservation), 'Sat') != 0)) {
                        $i++;
                        $dateExpiration = date('Y-m-d', $date_reservation);
                    }
                }else{

                    if ((strcmp(date('D', $date_reservation), 'Sun') != 0) || (strcmp(date('D', $date_reservation), 'Sat') != 0)) {
                        $i++;
                        $dateExpiration = date('Y-m-d', $date_reservation);
                    }
                }
            }
            return $dateExpiration;
        }else{

            $month = date('m',$date_reservation);
            $year = date('Y', $date_reservation);
            $expirationDay = date('d',$date_reservation);
            if($rules['limit_day'] > 0 && (int)$expirationDay > $rules['limit_day']){
                $changeMonth = date('m', strtotime("+1 month", $date_reservation));
                if($changeMonth < $month){
                    $year = date('Y', strtotime("+1 year", $date_reservation));
                }
                $month = $changeMonth;
            }
            $day = date("d", mktime(0,0,0, $month+1, 0, $year));
            $dateExpiration = date('Y-m-d', mktime(0,0,0, $month, $day, $year));

            if($rules['working_days']){
                $lastDay = strtotime($dateExpiration);
                for($lastDay;$lastDay>=$date_reservation;$lastDay=strtotime('-1 day ' . date('Y-m-d',$lastDay))){

                    if((strcmp(date('D',$lastDay),'Sun')!=0) && (strcmp(date('D',$lastDay),'Sat')!=0)){
                        return $dateExpiration = date('Y-m-d',$lastDay);
                    }
                }
            }
            return $dateExpiration;
        }
    }

}