<?php
require_once('DataBase.php');
include('Common.php');
class Airlines
{

    public function getAllAirlines()
    {
        try {
            $mysqli = DataBase::connex();
            $query = '
				    SELECT * FROM
				        rules_engine.airlines';
            $result = $mysqli->query($query);

            while ($row = $result->fetch_assoc()) {
                $airline['id'] = $row['airlines_id'];
                $airline['nombre'] = $row['name'];
                $airline['iata'] = $row['iata'];
                $airlines[] = $airline;
            }
            $mysqli->query($query);
            $mysqli->close();
            return Common::encodingJson($airlines);
        }catch (Exception $e){
            echo 'Se produjo este error'.$e;
        }


    }

}
