<?php
/**
 * Created by PhpStorm.
 * User: ebertoia
 */
require_once('DataBase.php');
include('Common.php');
include('Rules.php');
class Reservation
{
    public function insertReservation($reservation)
    {
        try {
            $reservation = $this->moldData($reservation);
            $mysqli = DataBase::connex();
            $query = '
                    INSERT INTO 
                        rules_engine.reservation
                    SET
                        reservation.reservation_id = NULL,
                        reservation.destination_to_id = "' . $mysqli->real_escape_string(trim($reservation['destination_to_id'])) . '",
                        reservation.destination_from_id = "' . $mysqli->real_escape_string(trim($reservation['destination_from_id'])) . '",
                        reservation.airline_id = "' . $mysqli->real_escape_string(trim($reservation['airline_id'])) . '",
                        reservation.date_to = "' . $mysqli->real_escape_string(trim($reservation['date_to'])) . '",
                        reservation.date_from = "' . $mysqli->real_escape_string(trim($reservation['date_from'])) . '",
                        reservation.date_reservation = "' . $mysqli->real_escape_string(trim($reservation['date_reservation'])) . '",
                        reservation.date_expiration = "' . $mysqli->real_escape_string(trim($reservation['date_expiration'])) . '"                            
                    ';
            $mysqli->query($query);
            $reservation['id'] = (string)mysqli_insert_id($mysqli);
            $reservation = array_diff($reservation, array(''));
            $mysqli->close();

            return $reservation;
        } catch (Exception $e) {
            echo 'se produjo este error' . $e;
        }
    }

    public function moldData($reservation)
    {
        include_once 'Rules.php';
        $rules = new Rules();
        $dates = explode(" - ", $reservation['datetimes']);
        $reservation['date_from'] = $this->dateFormat($dates[0]);
        $reservation['date_to'] = $this->dateFormat($dates[1]);
        $reservation['date_reservation'] = date('Y-m-d');
        $reservation['date_expiration'] = $rules->applicationOfRules($reservation);
        return $reservation;
    }

    public function dateFormat($date){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $date_from = DateTime::createFromFormat('Y-m-d H:i', $date);;
        return $date_from->format('Y-m-d H:i:s');
    }


    public function getAllReservation()
    {
        try {
            $mysqli = DataBase::connex();
            $query = '
				    SELECT * FROM
				        rules_engine.reservation';
            $result = $mysqli->query($query);

            while ($row = $result->fetch_assoc()) {
                $reservation['id'] = $row['reservation_id'];
                $reservation['destination_to_id'] = $row['destination_to_id'];
                $reservation['destination_from_id'] = $row['destination_from_id'];
                $reservation['airline_id'] = $row['airline_id'];
                $reservation['date_from'] = $row['date_from'];
                $reservation['date_to'] = $row['date_to'];
                $reservation['date_reservation'] = $row['date_reservation'];
                $reservation['date_expiration'] = $row['date_expiration'];
                $reservations[] = $reservation;
            }
            $mysqli->query($query);
            $mysqli->close();
            return Common::encodingJson($reservations);
        }catch (Exception $e){
            echo 'Se produjo este error'.$e;
        }


    }
}