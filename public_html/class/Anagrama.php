<?php
class Anagrama {

    protected $first;
    protected $second;

    public function __construct($aParams)
    {
        foreach ($aParams as $sParam => $mValue) {
            if(property_exists(get_class($this), $sParam)){
                $this->$sParam = $mValue;
            }
        }
    }

	public function str2array($str) {
        $tmp = [];
        for($i=0;$i<strlen($str);$i++) {
            $tmp[] = $str[$i];
        }
        return $tmp;
    }
    public function compareArrayValues($origin, $tocompare) {
        foreach($origin as $k=>$v) {
            $v2 = $tocompare[$k];
            if($v != $v2) return false;
        }
        return true;

    }
public function resultAction(){
    if(strlen($this->first) != strlen($this->second)) {
        return "Las palabras no coinciden en longitud.";
    } else {

        $s_first = $this->str2array($this->first);
        sort($s_first);
        $s_second = $this->str2array($this->second);
        sort($s_second);

        if($this->compareArrayValues($s_first,$s_second)) {
            return "Sí, son anagramas.";
        } else return "No, no son anagramas";

    }
}

}