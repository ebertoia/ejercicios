<?php
/**
 * Created by PhpStorm.
 * User: ebertoia
 */

include_once 'controllers/ViewController.php';
include_once 'controllers/PracticeController.php';
//include_once 'models/Greeting.php';

$action = isset($_GET['modul']) ? $_GET['modul'] : 'index';
$section = isset($_GET['e']) ? $_GET['e'] : '';
switch($section){
    case 'example':
        $controller = new PracticeController;
        break;
    default:
        $controller = new ViewController;
}

$controller->run($action);