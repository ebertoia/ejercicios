<?php
header('Content-type: application/json');


if (isset($_POST['getAllAirlines'])) {
    include_once 'class/Airlines.php';
    $airlines = new Airlines();
    echo json_encode($airlines->getAllAirlines());
}

if (isset($_POST['saveReservation'])) {
    include_once 'class/Reservation.php';
    $reservation = new Reservation();
    echo json_encode($reservation->insertReservation($_POST));
}
if (isset($_POST['getAllReservation'])) {
    include_once 'class/Reservation.php';
    $reservation = new Reservation();
    echo json_encode($reservation->getAllReservation());
}

if (isset($_POST['getAllRules'])) {
    include_once 'class/Rules.php';
    $rules = new Rules();
    echo json_encode($rules->getAllRules());
}

if(isset($_GET['first'])){
    include_once 'class/Anagrama.php';
    $anagrama = new Anagrama($_GET);
    echo json_encode($anagrama->resultAction());
}