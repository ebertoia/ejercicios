<html>
<head>
    <title > carga de vuelos </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="cache-control" content="no-cache, no-store">
    <meta http-equiv="pragma" content="no-cache">

    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/datePicker.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    


    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/controller.js"></script>
</head>
<body>
<div class='container'>
    <form  class="form-inline"  data-ajax='true'>
        <div class="well">
            <div class="form-group col-xs-4">
                <label for="destination_to">Buenos Aires - Miami </label>
                <input type="text" class="form-control" aria-describedby="basic-addon1" id="destination_from" name="destination_from_id" style="display: none" value="BUE"/>
                <input type="text" class="form-control" aria-describedby="basic-addon1" id="destination_to" name="destination_to_id" style="display: none" value="MIA"/>
            </div>
            <div class="form-group col-xs-4">
                <input type="text" class="date" name="datetimes" />
            </div>
            <div class="form-group col-xs-4">
                <label for="airline">Aerolineas</label>
                <select class="form-control" name="airline_id" id="airlines">

                </select>
            </div>
            <div class="form-group col-xs-4">
                <button  type="submit" name="saveReservation" style="width: 100%"  class="btn btn-default" >Guardar</button>
            </div>

        </div>
    </form>

    <table  class="table table-striped">
        <thead>
        <tr>
            <th>Id de reserva</th>
            <th>Ciudad Salida</th>
            <th>fecha/hora Salida</th>
            <th>Ciudad Llegada</th>
            <th>fecha/hora Llegada</th>
            <th>Aerolinea</th>
            <th>Vencimiento de Reserva</th>
        </tr>
        </thead>
        <tbody class="reservations">

        </tbody>
    </table>
</div>
</body>
