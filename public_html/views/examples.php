<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="cache-control" content="no-cache, no-store">
    <meta http-equiv="pragma" content="no-cache">

    <title >Ejercicios</title>
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" href="../css/font-awesome.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>

</head>
<body>
    <div class='container'>
        <div class="well">
            <h1>Ejercicios</h1>
            <a type="button" class="btn btn-primary" href="index.php?e=example&modul=exampleOne">Ejemplo 1</a>
            <a type="button" class="btn btn-primary" href="index.php?e=example&modul=exampleTwo">Ejemplo 2</a>
            <a type="button" class="btn btn-primary" href="index.php?e=example&modul=exampleThree">Ejemplo 3</a>
        </div>
    </div>
</body>