<html>
<head>
    <title>Ejercicios</title>
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/anagrama.js"></script>
</head>
<body>
<div class='container'>
    <div class="well">
        <form class="form-inline" data-ajax='true'>
            <div class="well">
                <div class="form-group col-xs-4">
                    <label for="destination_to">Determinar si dos strings son anagramas entre sí.</label>
                    <input type="text" class="form-control" required name="first" value=""/>
                    <input type="text" class="form-control" required name="second" value=""/>
                </div>
                <div class="responseAnagrama"></div>
            </div>
            <div class="form-group col-xs-4">
                <button  type="submit" style="width: 100%"  class="btn btn-default" >comparar</button>
            </div>
        </form>

    </div>
</div>
</body>