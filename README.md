
### Intro
Para levantar dicha aplicación necesitamos instalar Docker y docker compose.
https://docs.docker.com/install/linux/docker-ce/ubuntu/
https://docs.docker.com/compose/install/
Una vez hecho esto, dentro del repositorio ingresamos el siguiente comando.
Docker-compose up –build
Una vez que levanto el apache junto al php y al mysl corremos las migrations que estan dentro del repo.

Para correr las migration entramos al conteiner llamado mysql, con este comando podemos verlo
docker ps
Luego ingresamos al conteiner usando el conteiner id:
docker exec -it b34197e3c9d9 bash
Una vez dentro de conteiner entramos a la carptea /var/migrations/ y empezamos a correrlas
El usuario root  de mysql tiene esta clave rootpassword.

Una vez terminado todo esto:
La aplicacion esta lista para levantarse en http:localhost:8080
:)